import { createSlice } from "@reduxjs/toolkit";

const initialState = {

  email_otp: "",
  mobile_otp: "",
  email: "",
  mobile: "",
  type: "",
  r_key: "",
  vm: ""

}

export const userReducer = createSlice({
  name: "user",
  initialState,
  reducers: {
    clearState: (state, initialState) => {
      state = initialState
      return state;
    },
    EMAIL_OTP: (state, action) => {
      state.email_otp = action.payload?.email_otp
    },
    MOBILE_OTP: (state, action) => {
      state.mobile_otp = action.payload?.mobile_otp
    },
    User_Email: (state, action) => {
      state.email = action.payload?.email
    },
    User_Mobile: (state, action) => {
      state.mobile = action.payload?.mobile
    },
    Login_Type: (state, action) => {
      state.type = action.payload?.type
    },
    R_Key: (state, action) => {
      state.r_key = action.payload?.r_key
    },
    VM: (state, action) => {
      state.vm = action.payload?.vm
    },

  }
});

export default userReducer.reducer;
export const { clearState, EMAIL_OTP, MOBILE_OTP, User_Email, User_Mobile, Login_Type, R_Key, VM } = userReducer.actions;

