import React, { useState } from "react";
import ApiClass from "../../api/api";
import { useFormik } from "formik";
import * as Yup from "yup";
import SwalClass from "../../Common/Swal.js";
import { Link, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { R_Key, VM } from "../../Common/Redux/userReducer";

export default function VerificationPage() {
  const dispatch = useDispatch();

  const user = useSelector((state) => {
    return state.user;
  });
  let navigate = useNavigate();
  const [loading, setLoading] = useState(false);
  const [type, setType] = useState("password");
  const split_email = (data) => {
    if (data) {
      let s = data?.split("@");
      let a = s[0]?.slice(0, 2);
      let b = s[1]?.slice(s[1].indexOf("."));

      return `${a}*****${b}`;
    }
  };
  const formik = useFormik({
    initialValues: {
      password: "",
    },
    validationSchema: Yup.object({
      password: Yup.string().required("Password is required"),
    }),
    onSubmit: async (body) => {
      setLoading(true);
      body.type = user.type;
      body.value = body.type == "email" ? user.email : user?.mobile;

      let res = await ApiClass.postAuthNodeRequest("user/login", false, body);

      if (res.data?.status_code == 1) {
        setLoading(false);
        SwalClass.success(res.data.message);
        if (res.data.data.v == true) {
          dispatch(R_Key({ r_key: res?.data?.data?.r_key }));
          dispatch(VM({ vm: res?.data?.data?.vm }));
          navigate("/SecurityVerify");
        } else {
          navigate("/p2p");
        }
        return;
      } else {
        setLoading(false);
        return SwalClass.error(res.data.message);
      }
    },
  });

  return (
    <>
      <section className="auth_page d-flex align-items-center justify-content-center">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-md-8 col-lg-6 col-xl-4">
              <div className="main_auth_box py-5">
                <div className="auth_head mb-5">
                  <h3 className="mb-0 text-capitalize"> welcome back</h3>
                  <p className="mb-0"> {split_email(user?.email)} </p>
                </div>
                {/* <!-- auth head --> */}
                <div className="form_box">
                  <form className="row" onSubmit={formik.handleSubmit}>
                    <div className="col-md-12 col-lg-12 col-xl-12">
                      <div className="form_body mb-3">
                        <label
                          htmlFor="exampleInputEmail1"
                          className="form-label "
                        >
                          Password
                        </label>
                        <div className="input-group">
                          <input
                            type={type}
                            className="form-control shadow-none"
                            id="exampleInputEmail1"
                            name="password"
                            onChange={formik.handleChange}
                            value={formik.values.password}
                            aria-describedby="emailHelp"
                          />
                          {type == "password" ? (
                            <span
                              onClick={() => setType("text")}
                              className="input-group-text bg-transparent"
                            >
                              {" "}
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="24"
                                height="24"
                              >
                                <path d="M8.073 12.194 4.212 8.333c-1.52 1.657-2.096 3.317-2.106 3.351L2 12l.105.316C2.127 12.383 4.421 19 12.054 19c.929 0 1.775-.102 2.552-.273l-2.746-2.746a3.987 3.987 0 0 1-3.787-3.787zM12.054 5c-1.855 0-3.375.404-4.642.998L3.707 2.293 2.293 3.707l18 18 1.414-1.414-3.298-3.298c2.638-1.953 3.579-4.637 3.593-4.679l.105-.316-.105-.316C21.98 11.617 19.687 5 12.054 5zm1.906 7.546c.187-.677.028-1.439-.492-1.96s-1.283-.679-1.96-.492L10 8.586A3.955 3.955 0 0 1 12.054 8c2.206 0 4 1.794 4 4a3.94 3.94 0 0 1-.587 2.053l-1.507-1.507z"></path>
                              </svg>
                            </span>
                          ) : (
                            <span
                              onClick={() => setType("password")}
                              className="input-group-text bg-transparent"
                            >
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="24"
                                height="24"
                              >
                                <path d="M12 5c-7.633 0-9.927 6.617-9.948 6.684L1.946 12l.105.316C2.073 12.383 4.367 19 12 19s9.927-6.617 9.948-6.684l.106-.316-.105-.316C21.927 11.617 19.633 5 12 5zm0 11c-2.206 0-4-1.794-4-4s1.794-4 4-4 4 1.794 4 4-1.794 4-4 4z"></path>
                                <path d="M12 10c-1.084 0-2 .916-2 2s.916 2 2 2 2-.916 2-2-.916-2-2-2z"></path>
                              </svg>
                            </span>
                          )}
                        </div>
                        {formik.errors.password && formik.touched.password && (
                          <span
                            className="error-msg"
                            style={{
                              color: "rgb(242, 48, 81)",
                              fontSize: "12px",
                            }}
                          >
                            {formik.errors.password}
                          </span>
                        )}
                      </div>
                    </div>
                    {/* <!-- Password--> */}
                    <div className="col-md-12 col-lg-12 col-xl-12">
                      <div className="form_body mb-3">
                        {!loading ? (
                          <button
                            type="submit"
                            className="btn_next shadow-none border-0 w-100"
                          >
                            Login
                          </button>
                        ) : (
                          <button
                            className="btn_next shadow-none border-0 w-100"
                            type="button"
                          >
                            <span
                              className="spinner-grow spinner-grow-sm"
                              role="status"
                              aria-hidden="true"
                            ></span>
                            Loading...
                          </button>
                        )}
                      </div>
                    </div>
                    {/* <!-- btn --> */}
                    <div className="col-md-12 col-lg-12 col-xl-12">
                      <div className="form_body">
                        <label htmlFor="">
                          <p className="mb-0">
                            <Link
                              to="/resetpassword"
                              className="text-decoration-none"
                            >
                              Forgot Password ?
                            </Link>
                          </p>
                        </label>
                      </div>
                    </div>
                    {/* <!-- email/phone number --> */}
                  </form>
                </div>
                {/* <!-- form end --> */}
              </div>
            </div>
            {/* <!--===========col-md-12 col-lg-12 col-xl-12 ================== --> */}
          </div>
        </div>
      </section>
    </>
  );
}
