import React, { useState, useEffect } from "react";

{
  /*dynamic import*/
}
import { useNavigate } from "react-router-dom";
import ApiClass from "../../api/api.js";
import SwalClass from "../../Common/Swal.js";
import { useDispatch, useSelector } from "react-redux";
import { EMAIL_OTP, MOBILE_OTP } from "../../Common/Redux/userReducer";

import {
  onHandleKeyPress,
  onHandlePaste,
  onHandleKeyDown,
} from "../../Common/inputText.js";

{
  /*global variables*/
}
var get_email, get_mobile, email_interval_time, mobile_interval_time;

export default function PasswordSecurityVerify() {
  {
    /*navigate hook*/
  }
  let navigate = useNavigate();
  let dispatch = useDispatch();

  {
    /*localstorage data*/
  }

  {
    /*redux store type*/
  }
  const login_type = useSelector((state) => {
    return state?.user?.type;
  });
  const { email_otp, mobile_otp, r_key, vm } = useSelector((state) => {
    return state.user;
  });

  {
    /*states*/
  }
  const [loading, setLoading] = useState(false);
  const [validated, setValidated] = useState(false);
  const [emailCode, setEmailCode] = useState("email_code");
  const [mobileCode, setMobileCode] = useState("mobile_code");
  const [emailSpanLoader, setEmailSpanLoader] = useState(false);
  const [mobileSpanLoader, setMobileSpanLoader] = useState(false);
  const [emailInv, setEmailInv] = useState("");
  const [mobileInv, setMobileInv] = useState("");
  const [email, setEmail] = useState("");
  const [mobile, setMobile] = useState("");
  const [customErrorEmail, setCustomErrorEmail] = useState("");
  const [customErrorMobile, setCustomErrorMobile] = useState("");

  {
    /*key up check email and mobile otp length*/
  }
  const check_len = async () => {
    if (login_type == "email") {
      if (email?.length > 5) {
        setValidated(true);
        await new Promise((r) => setTimeout(r, 200));
        document.getElementById("form_sbt").click();
      } else {
        setValidated(false);
      }
    }

    if (login_type == "mobile") {
      if (mobile?.length > 5) {
        setValidated(true);
        await new Promise((r) => setTimeout(r, 200));
        document.getElementById("form_sbt").click();
      } else {
        setValidated(false);
      }
    }
  };

  {
    /*email otp expiry timer get*/
  }
  const emailTimer = async (val, type, btn) => {
    setEmailCode("email_code_send");
    dispatch(EMAIL_OTP({ email_otp: val }));
    val = new Date(val).getTime();
    let e_inv = setInterval(() => {
      var timeGap = val - new Date().getTime();
      var min = Math.floor((timeGap % (1000 * 60 * 60)) / (1000 * 60));
      var sec = Math.floor((timeGap % (1000 * 60)) / 1000);
      if (timeGap < 0) {
        clear(type);
        setEmailCode("resend_email_code");
      }
    }, 1000);
    setEmailInv(e_inv);
  };

  {
    /*mobile otp expiry timer get*/
  }
  const phoneTimer = async (val, type, btn) => {
    setMobileCode("mobile_code_send");
    dispatch(MOBILE_OTP({ mobile_otp: val }));

    val = new Date(val).getTime();
    let m_inv = setInterval(() => {
      var timeGap = val - new Date().getTime();
      var min = Math.floor((timeGap % (1000 * 60 * 60)) / (1000 * 60));
      var sec = Math.floor((timeGap % (1000 * 60)) / 1000);
      if (timeGap < 0) {
        this.clear(type);
        setMobileCode("resend_mobile_code");
      }
    }, 1000);
    setMobileInv(m_inv);
  };

  {
    /*interval time clear function*/
  }
  const clear = (type) => {
    type == "email" ? clearInterval(emailInv) : clearInterval(mobileInv);
  };

  {
    /*verify otp's reset form function*/
  }
  const resetForm = () => {
    setEmail("");
    setMobile("");
  };

  {
    /*get otp api integration for email and mobile*/
  }
  const get_otp = async (type, btn) => {
    type == "email" ? setEmailSpanLoader(true) : "";
    type == "mobile" ? setMobileSpanLoader(true) : "";
    let data = {
      side: "verify",
      type: type,
      r_key: r_key,
    };

    let res = await ApiClass.postAuthNodeRequest("user/get_code", false, data);

    if (res.data.status_code == 1) {
      type == "email" ? setEmailSpanLoader(false) : "";
      type == "mobile" ? setEmailSpanLoader(false) : "";
      type == "email"
        ? emailTimer(res?.data?.data?.expired_at, type, btn)
        : phoneTimer(res?.data?.data?.expired_at, type, btn);
      SwalClass.success(res.data.message);
    } else {
      SwalClass.error(res.data.message);
      if (res.data.r_key == "expired") {
        navigate("/login");
      }
      return;
    }
  };

  {
    /*verify otp api integration for email and mobile*/
  }
  const verify_otp = async (e) => {
    e.preventDefault();

    if (emailInv != "") {
      clearInterval(emailInv);
      setEmailCode("resend_email_code");
    }

    if (mobileInv != "") {
      clearInterval(mobileInv);
      setEmailCode("resend_email_code");
    }

    let data = {
      r_key: r_key,
      otps: {
        mobile: mobile,
        email: email,
        // "totp": totp
      },
    };
    setLoading(true);
    let res = await ApiClass.postAuthNodeRequest(
      "user/verify_otp",
      false,
      data
    );
    if (res.data.status_code == 1) {
      localStorage.setItem("token", res.data.token);
      localStorage.setItem("user", JSON.stringify(res.data.data.user));
      setEmail();
      setMobile();
      SwalClass.success(res.data.message);
      navigate("/p2p");
      return;
    }
    if (res.data.status_code == 0) {
      setLoading(false);
      SwalClass.error(res.data.message);
      if (res.data.r_key == "expired") {
        navigate("/login");
      }
      return;
    }
  };

  const email_r = useSelector((state) => {
    return state?.user?.email;
  });

  const mobile_r = useSelector((state) => {
    return state?.user?.mobile;
  });

  useEffect(() => {
    email_interval_time = localStorage.getItem("SET_EMAIL_OTP");
    mobile_interval_time = localStorage.getItem("SET_MOBILE_OTP");
    get_email = login_type == "email" ? email_r : "";
    get_mobile = login_type == "mobile" ? mobile_r : "";

    if (email_interval_time) {
      if (email_interval_time < Date.now()) {
        setEmailCode("resend_email_code");
      }

      if (email_interval_time > Date.now()) {
        setEmailCode("email_code_send");
        emailTimer(email_interval_time, "email");
      }
    }
    if (mobile_interval_time) {
      if (mobile_interval_time < Date.now()) {
        setMobileCode("resend_mobile_code");
      }

      if (mobile_interval_time > Date.now()) {
        setMobileCode("mobile_code_send");
        phoneTimer(mobile_interval_time, "mobile");
      }
    }
  }, []);

  useEffect(() => {
    if (!email) {
      setCustomErrorEmail("Email OTP required");
    } else {
      setCustomErrorEmail("");
    }

    if (!mobile) {
      setCustomErrorMobile("Mobile OTP required");
    } else {
      setCustomErrorMobile(" ");
    }
  }, [email, mobile]);

  return (
    <>
      <section className="auth_page d-flex align-items-center justify-content-center">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-md-8 col-lg-6 col-xl-4">
              <div className="main_auth_box py-5">
                <div className="auth_head mb-4">
                  <h3 className="mb-0">Security Verification</h3>
                  <span>
                    To secure your account,Please complete the following
                    verification.
                  </span>
                </div>
                {/* <!-- auth head --> */}
                <div className="form_box">
                  <form className="row" onSubmit={(e) => verify_otp(e)}>
                    {login_type == "email" ? (
                      <div
                        className="col-md-12 col-lg-12 col-xl-12"
                        v-if="login_type == 'email'"
                      >
                        <div className="form_body mb-4">
                          <label
                            htmlFor="exampleInputEmail1"
                            className="form-label "
                          >
                            Email Verification Code
                          </label>
                          <div className="input-group">
                            <input
                              type="text"
                              className="form-control shadow-none border-end-0"
                              name="email"
                              aria-label="Username"
                              aria-describedby="basic-addon1"
                              value={email}
                              onChange={(e) => setEmail(e.target.value)}
                              onKeyPress={(e) => onHandleKeyPress(e)}
                              onKeyUp={(e) => check_len(e)}
                              onKeyDown={(e) => onHandleKeyDown(e)}
                              onDragOver={(e) => onHandleKeyPress(e)}
                              onPaste={(e) => e.preventDefault(e)}
                            />
                            {emailCode == "email_code" && !emailSpanLoader ? (
                              <span
                                className="input-group-text bg-transparent"
                                id="basic-addon1"
                                onClick={() => get_otp("email", "email_code")}
                              >
                                Get Code
                              </span>
                            ) : (
                              ""
                            )}
                            {/* //  <!-- Verfication Code --> */}
                            {emailCode == "email_code_send" &&
                            !emailSpanLoader ? (
                              <span
                                className="input-group-text bg-transparent"
                                id="basic-addon1"
                              >
                                Verification code send
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  className="ms-1"
                                  height="14"
                                  width="14"
                                  style={{ fill: "rgb(112, 122, 138)" }}
                                  viewBox="0 0 512 512"
                                >
                                  <path d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM216 336h24V272H216c-13.3 0-24-10.7-24-24s10.7-24 24-24h48c13.3 0 24 10.7 24 24v88h8c13.3 0 24 10.7 24 24s-10.7 24-24 24H216c-13.3 0-24-10.7-24-24s10.7-24 24-24zm40-208a32 32 0 1 1 0 64 32 32 0 1 1 0-64z" />
                                </svg>
                              </span>
                            ) : (
                              ""
                            )}
                            {/* <!-- Resend Code --> */}
                            {emailCode == "resend_email_code" &&
                            !emailSpanLoader ? (
                              <span
                                className="input-group-text bg-transparent"
                                id="basic-addon1"
                                onClick={() =>
                                  get_otp("email", "resend_email_code")
                                }
                              >
                                Resend Code
                              </span>
                            ) : (
                              ""
                            )}
                            {emailSpanLoader ? (
                              <span
                                className="input-group-text bg-transparent"
                                id="basic-addon1"
                              >
                                <div className="spinner-border" role="status">
                                  <span className="visually-hidden">
                                    Loading...
                                  </span>
                                </div>
                              </span>
                            ) : (
                              ""
                            )}
                          </div>
                          <span
                            style={{
                              color: "rgb(242, 48, 81)",
                              fontSize: "small",
                            }}
                          >
                            {customErrorEmail}
                          </span>
                        </div>
                      </div>
                    ) : (
                      <div className="col-md-12 col-lg-12 col-xl-12">
                        <div className="form_body mb-4">
                          <label
                            htmlFor="exampleInputEmail1"
                            className="form-label "
                          >
                            Phone Number Verification Code
                          </label>
                          <div className="input-group">
                            <input
                              type="text"
                              className="form-control shadow-none border-end-0"
                              name="mobile"
                              aria-label="Username"
                              aria-describedby="basic-addon1"
                              onChange={(e) => setMobile(e.target.value)}
                              value={mobile}
                              onKeyPress={(e) => onHandleKeyPress(e)}
                              onKeyUp={(e) => check_len(e)}
                              onKeyDown={(e) => onHandleKeyDown(e)}
                              onDragOver={(e) => onHandleKeyPress(e)}
                              onPaste={(e) => onHandlePaste(e)}
                            />
                            {mobileCode == "mobile_code" &&
                            !mobileSpanLoader ? (
                              <span
                                className="input-group-text bg-transparent"
                                id="basic-addon1"
                                onClick={() => get_otp("mobile", "mobile_code")}
                              >
                                Get Code
                              </span>
                            ) : (
                              ""
                            )}

                            {/* //  <!-- Verfication Code --> */}
                            {mobileCode == "mobile_code_send" &&
                            !mobileSpanLoader ? (
                              <span
                                className="input-group-text bg-transparent"
                                id="basic-addon1"
                              >
                                Verification code send
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  className="ms-1"
                                  height="14"
                                  width="14"
                                  style={{ fill: "rgb(112, 122, 138)" }}
                                  viewBox="0 0 512 512"
                                >
                                  <path d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM216 336h24V272H216c-13.3 0-24-10.7-24-24s10.7-24 24-24h48c13.3 0 24 10.7 24 24v88h8c13.3 0 24 10.7 24 24s-10.7 24-24 24H216c-13.3 0-24-10.7-24-24s10.7-24 24-24zm40-208a32 32 0 1 1 0 64 32 32 0 1 1 0-64z" />
                                </svg>
                              </span>
                            ) : (
                              ""
                            )}

                            {/* <!-- Resend Code --> */}
                            {mobileCode == "resend_mobile_code" &&
                            !mobileSpanLoader ? (
                              <span
                                className="input-group-text bg-transparent"
                                id="basic-addon1"
                                onClick={() =>
                                  get_otp("mobile", "resend_mobile_code")
                                }
                              >
                                Resend Code
                              </span>
                            ) : (
                              ""
                            )}
                            {mobileSpanLoader ? (
                              <span
                                className="input-group-text bg-transparent"
                                id="basic-addon1"
                              >
                                <div className="spinner-border" role="status">
                                  <span className="visually-hidden">
                                    Loading...
                                  </span>
                                </div>
                              </span>
                            ) : (
                              ""
                            )}
                          </div>
                          <span
                            style={{
                              color: "rgb(242, 48, 81)",
                              fontSize: "small",
                            }}
                          >
                            {customErrorMobile}
                          </span>
                        </div>
                      </div>
                    )}

                    <div className="col-md-12 col-lg-12 col-xl-12">
                      <div className="form_body mb-2">
                        {!validated && !loading ? (
                          <button
                            type="button"
                            className="btn_next shadow-none border-0 w-100"
                            style={{
                              opacity: "0.5",
                              cursor: "not-allowed",
                              pointerEvents: "all",
                            }}
                            disabled
                          >
                            {" "}
                            submit
                          </button>
                        ) : (
                          <>
                            {validated && loading ? (
                              <button
                                className="btn_next shadow-none border-0 w-100"
                                type="button"
                              >
                                <span
                                  className="spinner-grow spinner-grow-sm"
                                  role="status"
                                  aria-hidden="true"
                                ></span>
                                Loading...
                              </button>
                            ) : (
                              <button
                                type="submit"
                                id="form_sbt"
                                className="btn_next shadow-none border-0 w-100"
                              >
                                submit
                              </button>
                            )}
                          </>
                        )}
                      </div>
                    </div>
                  </form>
                </div>
                {/* //  <!-- form end --> */}
              </div>
            </div>
            {/* //  <!--===========col-md-12 col-lg-12 col-xl-12 ================== --> */}
          </div>
        </div>
      </section>
    </>
  );
}
