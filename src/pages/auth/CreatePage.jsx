import React, { useState } from "react";
import ApiClass from "../../api/api";
import { useFormik } from "formik";
import * as Yup from "yup";
import SwalClass from "../../Common/Swal.js";
import { Link, useNavigate } from "react-router-dom";
import { useEffect } from "react";

export default function CreatePage() {
  const [loginBy, setLoginBy] = useState("email");
  const [type, setType] = useState("password");
  const [loading, setLoading] = useState(false);
  const validateEmail = (email) => {
    return Yup.string().email().isValidSync(email);
  };
  const validatePhone = (phone) => {
    return Yup.number()
      .integer()
      .positive()
      .test((phone) => {
        return phone &&
          phone.toString().length >= 8 &&
          phone.toString().length <= 14
          ? true
          : false;
      })
      .isValidSync(phone);
  };

  const formik = useFormik({
    initialValues: {
      email: "",
      mobile: "",
      password: "",
      terms: "",
    },
    validationSchema: Yup.object({
      email: Yup.string()
        .required(`${loginBy} is required`)
        .test("email_or_phone", `${loginBy} is invalid`, (value) => {
          if (loginBy == "email") {
            return validateEmail(value);
          } else {
            return validatePhone(parseInt(value ?? "0"));
          }
        }),
      password: Yup.string().required("Password is required"),
      terms: Yup.boolean()
        .oneOf([true], "You have To Agree Term & Condition")
        .required("You have To Agree Term & Condition"),
    }),
    onSubmit: async (body) => {
      setLoading(true);
      body.type = loginBy;
      if (loginBy == "mobile") {
        body.mobile = body.email;
        body.email = "";
      }
      if (loginBy == "email") {
        body.email;
        body.mobile = "";
      }
      let res = await ApiClass.postAuthNodeRequest("user/signup", false, body);
      if (res.data?.status_code == 1) {
        setLoading(false);
        SwalClass.success(res.data.message);
        document.getElementById("terms").checked = false;
        formik.resetForm();
        navigate("/login");
      } else {
        setLoading(false);
        SwalClass.error(res.data.message);
      }
    },
  });
  useEffect(() => {
    document.getElementById("terms").checked = false;
    formik.resetForm();
  }, [loginBy]);
  return (
    <>
      <section className="auth_page d-flex align-items-center justify-content-center">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-md-8 col-lg-6 col-xl-4">
              <div className="main_auth_box py-5">
                <div className="auth_head mb-4">
                  <h3 className="mb-0">Create Personal Account</h3>
                </div>
                {/* <!-- auth head --> */}
                <div className="btn_tabs_box d-flex gap-4 mb-4">
                  <button
                    onClick={() => setLoginBy("email")}
                    style={
                      loginBy == "email"
                        ? { backgroundColor: "var(--filter-bg)" }
                        : {}
                    }
                    type="button"
                    className="btn_tabs shadow-none border-0"
                  >
                    Email
                  </button>
                  <button
                    onClick={() => setLoginBy("mobile")}
                    style={
                      loginBy == "mobile"
                        ? { backgroundColor: "var(--filter-bg)" }
                        : {}
                    }
                    type="button"
                    className="btn_tabs shadow-none border-0"
                  >
                    Phone Number
                  </button>
                </div>
                {/* //  <!-- tabs div --> */}
                <div className="form_box">
                  <form className="row" onSubmit={formik.handleSubmit}>
                    {loginBy == "email" ? (
                      <div className={`col-md-12 col-lg-12 col-xl-12 `}>
                        <div className="form_body mb-4">
                          <label
                            htmlFor="exampleInputEmail1"
                            className="form-label "
                          >
                            Personal Email
                          </label>
                          <input
                            type="email"
                            className="form-control shadow-none"
                            id="exampleInputEmail1"
                            onPaste={(e) => e.preventDefault()}
                            aria-describedby="emailHelp"
                            name="email"
                            onChange={formik.handleChange}
                            value={formik.values.email}
                          />
                          {formik.errors.email && formik.touched.email && (
                            <span
                              className="error-msg"
                              style={{
                                color: "rgb(242, 48, 81)",
                                fontSize: "12px",
                              }}
                            >
                              {formik.errors.email}
                            </span>
                          )}
                        </div>
                      </div>
                    ) : (
                      <div className={`col-md-12 col-lg-12 col-xl-12 `}>
                        <div className="form_body mb-4">
                          <label
                            htmlFor="exampleInputEmail1"
                            className="form-label "
                          >
                            Personal Phone Number
                          </label>
                          <input
                            type="text"
                            className="form-control shadow-none"
                            id="exampleInputEmail1"
                            name="email"
                            onChange={formik.handleChange}
                            value={formik.values.email}
                            aria-describedby="emailHelp"
                            onPaste={(e) => e.preventDefault()}
                          />

                          {formik.errors.email && formik.touched.email && (
                            <span
                              className="error-msg"
                              style={{
                                color: "rgb(242, 48, 81)",
                                fontSize: "12px",
                              }}
                            >
                              {formik.errors.email}
                            </span>
                          )}
                        </div>
                      </div>
                    )}
                    {/* // <!-- phone number Email --> */}
                    <div className="col-md-12 col-lg-12 col-xl-12">
                      <div className="form_body mb-3">
                        <label
                          htmlFor="exampleInputEmail1"
                          className="form-label "
                        >
                          Password
                        </label>
                        <div className="input-group">
                          <input
                            type={type}
                            className="form-control shadow-none border-end-0"
                            aria-label="Username"
                            name="password"
                            onChange={formik.handleChange}
                            value={formik.values.password}
                            aria-describedby="basic-addon1"
                            onPaste={(e) => e.preventDefault()}
                          />
                          {type == "password" ? (
                            <span
                              onClick={() => setType("text")}
                              className="input-group-text bg-transparent"
                            >
                              {" "}
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="24"
                                height="24"
                                style={{
                                  fill: "rgba(183, 183, 183, 1)",
                                  fontSize: "12px",
                                }}
                              >
                                <path d="M8.073 12.194 4.212 8.333c-1.52 1.657-2.096 3.317-2.106 3.351L2 12l.105.316C2.127 12.383 4.421 19 12.054 19c.929 0 1.775-.102 2.552-.273l-2.746-2.746a3.987 3.987 0 0 1-3.787-3.787zM12.054 5c-1.855 0-3.375.404-4.642.998L3.707 2.293 2.293 3.707l18 18 1.414-1.414-3.298-3.298c2.638-1.953 3.579-4.637 3.593-4.679l.105-.316-.105-.316C21.98 11.617 19.687 5 12.054 5zm1.906 7.546c.187-.677.028-1.439-.492-1.96s-1.283-.679-1.96-.492L10 8.586A3.955 3.955 0 0 1 12.054 8c2.206 0 4 1.794 4 4a3.94 3.94 0 0 1-.587 2.053l-1.507-1.507z"></path>
                              </svg>
                            </span>
                          ) : (
                            <span
                              onClick={() => setType("password")}
                              className="input-group-text bg-transparent"
                            >
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="24"
                                height="24"
                                style={{
                                  fill: "rgba(183, 183, 183, 1)",
                                  fontSize: "12px",
                                }}
                              >
                                <path d="M12 5c-7.633 0-9.927 6.617-9.948 6.684L1.946 12l.105.316C2.073 12.383 4.367 19 12 19s9.927-6.617 9.948-6.684l.106-.316-.105-.316C21.927 11.617 19.633 5 12 5zm0 11c-2.206 0-4-1.794-4-4s1.794-4 4-4 4 1.794 4 4-1.794 4-4 4z"></path>
                                <path d="M12 10c-1.084 0-2 .916-2 2s.916 2 2 2 2-.916 2-2-.916-2-2-2z"></path>
                              </svg>
                            </span>
                          )}
                        </div>
                        {formik.errors.password && formik.touched.password && (
                          <span
                            className="error-msg"
                            style={{
                              color: "rgb(242, 48, 81)",
                              fontSize: "12px",
                            }}
                          >
                            {formik.errors.password}
                          </span>
                        )}
                      </div>
                    </div>

                    <div className="col-md-12 col-lg-12 col-xl-12">
                      <div className="form_body mb-3">
                        <div className="form-check">
                          <input
                            className="form-check-input shadow-none"
                            type="checkbox"
                            name="terms"
                            id="terms"
                            onChange={formik.handleChange}
                            value={formik.values.terms}
                          />
                          <label
                            className="form-check-label"
                            htmlFor="flexCheckDefault"
                          >
                            <p>
                              I have read and agree to Binance’s{" "}
                              <a href="">Terms of Service</a> and{" "}
                              <a href="">Privacy Policy</a>.
                            </p>
                          </label>
                        </div>
                        {formik.errors.terms && formik.touched.terms && (
                          <span
                            className="error-msg"
                            style={{
                              color: "rgb(242, 48, 81)",
                              fontSize: "12px",
                            }}
                          >
                            {formik.errors.terms}
                          </span>
                        )}
                      </div>
                    </div>
                    {/* // <!-- btn --> */}
                    <div className="col-md-12 col-lg-12 col-xl-12">
                      <div className="form_body mb-3">
                        {!loading ? (
                          <button
                            type="submit"
                            className="btn_next shadow-none border-0 w-100"
                          >
                            Create Personal Account
                          </button>
                        ) : (
                          <button
                            className="btn_next shadow-none border-0 w-100"
                            type="button"
                          >
                            <span
                              className="spinner-grow spinner-grow-sm"
                              role="status"
                              aria-hidden="true"
                            ></span>
                            Loading...
                          </button>
                        )}
                      </div>
                    </div>
                    {/* // <!-- btn --> */}
                    <div className="col-md-12 col-lg-12 col-xl-12">
                      <div className="form_body">
                        <label htmlFor="">
                          <p className="mb-0">
                            Not looking for a personal account?{" "}
                            <a href="" className="text-decoration-none">
                              Sign up for an entity account
                            </a>
                          </p>
                        </label>
                      </div>
                    </div>
                    {/* // <!-- email/phone number --> */}
                  </form>
                </div>
                {/* //  <!-- form end --> */}
              </div>
            </div>
            {/* //  <!--===========col-md-12 col-lg-12 col-xl-12 ================== --> */}
          </div>
        </div>
      </section>
    </>
  );
}
