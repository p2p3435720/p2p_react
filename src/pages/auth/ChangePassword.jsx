import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

{
  /*dynamic import*/
}
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import ApiClass from "../../api/api.js";
import SwalClass from "../../Common/Swal.js";
import { useSelector } from "react-redux";

var reset_type, user_email;

export default function ChangePassword() {
  {
    /*navigate to another page*/
  }
  let navigate = useNavigate();

  {
    /*reduc store email type mobile*/
  }
  const login_type = useSelector((state) => {
    return state?.user?.type;
  });

  const email_r = useSelector((state) => {
    return state?.user?.email;
  });

  const mobile_r = useSelector((state) => {
    return state?.user?.mobile;
  });

  {
    /*states*/
  }
  const [type, setType] = useState("password");
  const [c_type, setC_type] = useState("password");
  const [loading, setLoading] = useState(false);

  {
    /*input handel function*/
  }
  const handelToggel = () => {
    if (type === "password") {
      setType("text");
    } else {
      setType("password");
    }
  };

  const handelToggel_Con = () => {
    if (c_type === "password") {
      setC_type("text");
    } else {
      setC_type("password");
    }
  };

  {
    /*useFormik validations*/
  }
  const { errors, touched, handleChange, handleSubmit, values, resetForm } =
    useFormik({
      initialValues: {
        _password: "",
        confirm_password: "",
      },
      validationSchema: Yup.object({
        _password: Yup.string().required("Password is required"),
        confirm_password: Yup.string()
          .oneOf([Yup.ref("_password")], "Confirm Password should be same")
          .required("Confirm Password is Required"),
      }),
      // API CALLING
      onSubmit: async (body) => {
        setLoading(true);
        let data = {
          reset_type: login_type,
          email: login_type == "email" ? email_r : "",
          mobile: login_type == "mobile" ? mobile_r : "",
          new_password: body._password,
          confirm_password: body.confirm_password,
        };
        const response = await ApiClass.postAuthNodeRequest(
          "user/reset",
          false,
          data
        );
        if (response === undefined) {
          setLoading(false);
          SwalClass.error("404 NOT FOUND");
          resetForm();
          return;
        }
        if (response?.data?.status_code == 0) {
          setLoading(false);
          SwalClass.error(response?.data?.message);
          resetForm();
          return;
        }
        if (response.data.status_code == 1) {
          resetForm();
          SwalClass.success(response?.data?.message);
          navigate("/");
        }
      },
    });

  useEffect(() => {
    reset_type = "email";
    user_email = email_r;
    // reset_type = this.$store.getters.getLoginType;
    // user_email = reset_type == "email" ? this.$store.getters.getUserEmail: "";
    // reset_type == "mobile" ? this.$store.getters.getUserMobile : "";
  }, []);

  return (
    <>
      <section className="auth_page d-flex align-items-center justify-content-center">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-md-8 col-lg-6 col-xl-4">
              <div className="main_auth_box py-5">
                <div className="auth_head mb-5">
                  <h3 className="mb-0 text-capitalize"> Reset Your Password</h3>
                </div>
                {/* <!-- auth head --> */}
                <div className="form_box">
                  <form className="row" onSubmit={handleSubmit}>
                    {/* <!-- Password--> */}
                    <div className="col-md-12 col-lg-12 col-xl-12">
                      <div className="form_body mb-3">
                        <label
                          htmlFor="exampleInputEmail1"
                          className="form-label "
                        >
                          New Password
                        </label>
                        <div className="input-group">
                          <input
                            className="form-control shadow-none border-end-0 "
                            aria-label="Username"
                            aria-describedby="basic-addon1"
                            type={type}
                            name="_password"
                            value={values._password}
                            onChange={handleChange}
                          />
                          {type == "password" ? (
                            <span
                              className="input-group-text bg-transparent"
                              onClick={handelToggel}
                            >
                              {" "}
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="24"
                                height="24"
                                style={{ fill: "rgb(183, 183, 183, 1)" }}
                              >
                                <path d="M8.073 12.194 4.212 8.333c-1.52 1.657-2.096 3.317-2.106 3.351L2 12l.105.316C2.127 12.383 4.421 19 12.054 19c.929 0 1.775-.102 2.552-.273l-2.746-2.746a3.987 3.987 0 0 1-3.787-3.787zM12.054 5c-1.855 0-3.375.404-4.642.998L3.707 2.293 2.293 3.707l18 18 1.414-1.414-3.298-3.298c2.638-1.953 3.579-4.637 3.593-4.679l.105-.316-.105-.316C21.98 11.617 19.687 5 12.054 5zm1.906 7.546c.187-.677.028-1.439-.492-1.96s-1.283-.679-1.96-.492L10 8.586A3.955 3.955 0 0 1 12.054 8c2.206 0 4 1.794 4 4a3.94 3.94 0 0 1-.587 2.053l-1.507-1.507z"></path>
                              </svg>
                            </span>
                          ) : (
                            <span
                              className="input-group-text bg-transparent"
                              onClick={handelToggel}
                            >
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="24"
                                height="24"
                                style={{ fill: "rgb(183, 183, 183, 1)" }}
                              >
                                <path d="M12 5c-7.633 0-9.927 6.617-9.948 6.684L1.946 12l.105.316C2.073 12.383 4.367 19 12 19s9.927-6.617 9.948-6.684l.106-.316-.105-.316C21.927 11.617 19.633 5 12 5zm0 11c-2.206 0-4-1.794-4-4s1.794-4 4-4 4 1.794 4 4-1.794 4-4 4z"></path>
                                <path d="M12 10c-1.084 0-2 .916-2 2s.916 2 2 2 2-.916 2-2-.916-2-2-2z"></path>
                              </svg>
                            </span>
                          )}
                        </div>
                        {errors._password && touched._password && (
                          <span
                            style={{
                              color: "rgb(242, 48, 81)  ",
                              fontSize: "small",
                            }}
                          >
                            {errors._password}
                          </span>
                        )}
                      </div>
                    </div>
                    {/* //   <!-- New Password--> */}

                    <div className="col-md-12 col-lg-12 col-xl-12">
                      <div className="form_body mb-3">
                        <label
                          htmlFor="exampleInputEmail1"
                          className="form-label "
                        >
                          Confirm Password
                        </label>
                        <div className="input-group">
                          <input
                            className="form-control shadow-none border-end-0 "
                            aria-label="Username"
                            aria-describedby="basic-addon1"
                            type={c_type}
                            name="confirm_password"
                            value={values.confirm_password}
                            onChange={handleChange}
                          />
                          {c_type == "password" ? (
                            <span
                              className="input-group-text bg-transparent"
                              onClick={handelToggel_Con}
                            >
                              {" "}
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="24"
                                height="24"
                                style={{ fill: "rgb(183, 183, 183, 1)" }}
                              >
                                <path d="M8.073 12.194 4.212 8.333c-1.52 1.657-2.096 3.317-2.106 3.351L2 12l.105.316C2.127 12.383 4.421 19 12.054 19c.929 0 1.775-.102 2.552-.273l-2.746-2.746a3.987 3.987 0 0 1-3.787-3.787zM12.054 5c-1.855 0-3.375.404-4.642.998L3.707 2.293 2.293 3.707l18 18 1.414-1.414-3.298-3.298c2.638-1.953 3.579-4.637 3.593-4.679l.105-.316-.105-.316C21.98 11.617 19.687 5 12.054 5zm1.906 7.546c.187-.677.028-1.439-.492-1.96s-1.283-.679-1.96-.492L10 8.586A3.955 3.955 0 0 1 12.054 8c2.206 0 4 1.794 4 4a3.94 3.94 0 0 1-.587 2.053l-1.507-1.507z"></path>
                              </svg>
                            </span>
                          ) : (
                            <span
                              className="input-group-text bg-transparent"
                              onClick={handelToggel_Con}
                            >
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="24"
                                height="24"
                                style={{ fill: "rgb(183, 183, 183, 1)" }}
                              >
                                <path d="M12 5c-7.633 0-9.927 6.617-9.948 6.684L1.946 12l.105.316C2.073 12.383 4.367 19 12 19s9.927-6.617 9.948-6.684l.106-.316-.105-.316C21.927 11.617 19.633 5 12 5zm0 11c-2.206 0-4-1.794-4-4s1.794-4 4-4 4 1.794 4 4-1.794 4-4 4z"></path>
                                <path d="M12 10c-1.084 0-2 .916-2 2s.916 2 2 2 2-.916 2-2-.916-2-2-2z"></path>
                              </svg>
                            </span>
                          )}
                        </div>
                        {errors.confirm_password &&
                          touched.confirm_password && (
                            <span
                              style={{
                                color: "rgb(242, 48, 81)  ",
                                fontSize: "small",
                              }}
                            >
                              {errors.confirm_password}
                            </span>
                          )}
                      </div>
                    </div>
                    {/* // <!-- Buttons--> */}
                    <div className="col-md-12 col-lg-12 col-xl-12">
                      <div className="form_body mb-3">
                        {loading ? (
                          <button
                            hidden
                            className="btn_next shadow-none border-0 w-100"
                            type="button"
                          >
                            <span
                              className="spinner-grow spinner-grow-sm"
                              role="status"
                              aria-hidden="true"
                            ></span>
                            Loading...
                          </button>
                        ) : (
                          <button
                            type="submit"
                            className="btn_next shadow-none border-0 w-100"
                          >
                            Login
                          </button>
                        )}
                      </div>
                    </div>
                    {/* // <!-- btn --> */}
                    <div className="col-md-12 col-lg-12 col-xl-12">
                      <div className="form_body">
                        <label htmlFor="">
                          <p className="mb-0">
                            <Link
                              to="/resetpassword"
                              className="text-decoration-none"
                            >
                              Forgot Password ?
                            </Link>
                          </p>
                        </label>
                      </div>
                    </div>
                    {/* // <!-- email/phone number --> */}
                  </form>
                </div>
                {/* //  <!-- form end --> */}
              </div>
            </div>
            {/* //  <!--===========col-md-12 col-lg-12 col-xl-12 ================== --> */}
          </div>
        </div>
      </section>
    </>
  );
}
