import { Routes, Route } from "react-router-dom";
import AdvertiserDetails from "../pages/AdvertiserDetails";
import MyAds from "../pages/MyAds";
import OrderDetail from "../pages/OrderDetail";
import OrderStatus from "../pages/OrderStatus";
import Order from "../pages/Order";
import P2P from "../pages/P2P";
import PostNormal from "../pages/PostNormal";
import Payment from "../pages/Payment";
import PaytmPayment from "../pages/PaytmPayment";
import IMPSPayment from "../pages/IMPSPayment";
import Login from "../pages/Login";
import ChangePassword from "../pages/auth/ChangePassword";
import CreatePage from "../pages/auth/CreatePage";
import LoginPage from "../pages/auth/LoginPage";
import PasswordSecurityVerify from "../pages/auth/PasswordSecurityVerify";
import ResetPassword from "../pages/auth/ResetPassword";
import SecurityVerify from "../pages/auth/SecurityVerify";
import VerificationPage from "../pages/auth/VerificationPage";

export default function AppRoutes() {
  // Routes for Components
  const routes = [
    // {
    //   path: "/",
    //   name: "Login",
    //   Component: Login,
    // },
    {
      path: "/p2p",
      name: "p2p",
      Component: P2P,
    },
    {
      path: "/orderdetail",
      name: "orderdetail",
      Component: OrderDetail,
    },
    {
      path: "/orderstatus",
      name: "orderstatus",
      Component: OrderStatus,
    },
    {
      path: "/orders",
      name: "orders",
      Component: Order,
    },
    {
      path: "/advertiser-details",
      name: "advertiser-details",
      Component: AdvertiserDetails,
    },
    {
      path: "/postnormal",
      name: "Post Normal",
      Component: PostNormal,
    },
    {
      path: "/myads",
      name: "My Ads",
      Component: MyAds,
    },
    {
      path: "/payment",
      name: "Payment",
      Component: Payment,
    },
    {
      path: "/paytm",
      name: "Paytm Payment",
      Component: PaytmPayment,
    },
    {
      path: "/addpayment",
      name: "IMPS Payment",
      Component: IMPSPayment,
    },
    // auth
    {
      path: "/changepassword",
      name: "ChangePassword",
      Component: ChangePassword,
    },
    {
      path: "/createpage",
      name: "CreatePage",
      Component: CreatePage,
    },
    {
      path: "/",
      name: "LoginPage",
      Component: LoginPage,
    },
    {
      path: "/passwordsecurity",
      name: "PasswordSecurityVerify",
      Component: PasswordSecurityVerify,
    },
    {
      path: "/resetpassword",
      name: "ResetPassword",
      Component: ResetPassword,
    },
    {
      path: "/securityverify",
      name: "SecurityVerify",
      Component: SecurityVerify,
    },
    {
      path: "/verification",
      name: "VerificationPage",
      Component: VerificationPage,
    },
  ];

  // ROUTES MAPING
  const Routing = routes.map(({ name, path, Component }, i) => (
    <Route key={i} path={path} element={<Component />} />
  ));

  return (
    <div className="">
      <Routes>{Routing}</Routes>
    </div>
  );
}
